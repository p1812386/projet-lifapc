#include "dijkstra.h"

using namespace std;

float a = std::numeric_limits<float>::infinity();

Dijkstra::Dijkstra(Graph *g){
    tailleGrille = g->getTailleGrille();
    dji = new infoIndex[g->getTailleGrille()];

    for (int i=0; i<tailleGrille; i++){
            
            dji[i].dist = a;
            dji[i].pred = 0;
            dji[i].couleur = "blanc";
        
        }
}





bool Dijkstra::present(Graph *g, string col){
    bool present = false;
    for(int i=0; i<tailleGrille; i++){
        if(dji[i].couleur == col)
        {
            present = true;
            break;
        }
    }
    return present;
}




void Dijkstra::dijkstraGraph(Graph *g){
    

    float val = a;
    int size = 0;

    cout<<"entrez le nombre des librairies : "<<endl;
    cin>>size;
    int lib[size];
    int j;
    cout<<"donnez les librairies : "<<endl;
    for(int i = 0; i < size; i++){
        cin>>j;
        lib[i] = j;
    }

    
    for(int j=0; j < size; j++){

        for (int i=0; i<tailleGrille; i++){
            
            dji[i].couleur = "blanc";
            dji[lib[j]].couleur = "gris"; 
            


            if(g->if_voisin(lib[j], i) ) {
                    
                val = g->valuationVoisin(lib[j], i);
                    if (dji[i].dist > val) {

                        dji[i].dist = val;
                        dji[i].pred = lib[j];
                        dji[i].couleur = "gris";
                    }
                   
            }
            
            dji[lib[j]].couleur = "noir";
            dji[lib[j]].dist = 0;
            dji[lib[j]].pred = lib[j];

            
        }

        while(present(g, "gris") && size != 0){

            float distMin = a;
            int nMin;

            for(int i = 0; i<tailleGrille; i++){
                if(dji[i].couleur == "gris"){
                    if(dji[i].dist < distMin){
                        distMin = dji[i].dist;
                        nMin = i;
                    }
                }
            }

            
            for(int i = 0; i<4; i++){
                if(g->if_voisin(nMin, g->voisin(nMin, i))) {
                    
                    int v = g->voisin(nMin, i);

                    if(dji[v].couleur == "blanc") {
                        dji[v].couleur = "gris";
                    }

                    if(dji[nMin].dist + g->valuationVoisin(nMin, v) < dji[v].dist){

                        dji[v].dist = dji[nMin].dist + g->valuationVoisin(nMin, v);
                        dji[v].pred = nMin;
                    }
                }
            
            }
        
            dji[nMin].couleur = "noir";
        }

        
    }

    for(int j=0; j < size; j++){
        voronoi(lib[j]);
        cout<<endl;       
    }

}




    void Dijkstra::affichageTabPcd(Graph *g) const{
        for(int i=0; i<tailleGrille; i++){
            cout<<"la distance a partir de "<<i<< " est "<<dji[i].dist<<endl;
            cout<<"le predecesseur de "<<i<<" est "<<dji[i].pred<<endl;
            cout<<"la couleur de "<<i<<" est "<<dji[i].couleur<<endl;
            cout<<endl;
        }
    }



    int Dijkstra::trouveDepart(int index){
        if(dji[index].pred != index)
        {
            trouveDepart(dji[index].pred);
        }
        else{return index;}
        
        
            
    }


    void Dijkstra::voronoi(int libV){

        for(int i=0; i<tailleGrille; i++){
            if (libV == trouveDepart(i)){
                cout<< libV <<" ====> " << i <<endl;
            }

        }
        
    }



    Dijkstra::~Dijkstra(){
        delete [] dji;
    }





