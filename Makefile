grille : main.o graph.o dijkstra.o
	g++ -Wall -ggdb main.o graph.o dijkstra.o -o grille

dijkstra.o : dijkstra.cpp dijkstra.h
	g++ -Wall -ggdb -c dijkstra.cpp dijkstra.h

graph.o : graph.cpp graph.h
	g++ -Wall -ggdb -c graph.cpp

main.o : main.cpp 
	g++ -Wall -ggdb -c main.cpp


clean :
	rm *.o
	rm *.gch