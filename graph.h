#ifndef _SOMMET
#define _SOMMET
#include<math.h>
#include<string> 
#include <cassert>
#include <fstream>
#include <limits>


using namespace std;



class Graph{
    public :
        //Graph();
        Graph(int ligne, int colonne);
        Graph(const std::string & filename);
        ~Graph();

        int trouveIndex (int l, int c) const;
        int trouveLigne(int index) const;
        int trouveColonne(int index) const;

        int voisinEst(int index) const;
        int voisinOuest(int index) const;
        int voisinNord(int index) const;
        int voisinSud(int index) const;

        float voisin(int index, int dir);

        float valuationEst(int index) const;
        float valuationOuest(int index) const;
        float valuationNord(int index) const;
        float valuationSud(int index) const;

        bool if_voisin(int indexA, int indexB);
        float valuationVoisin(int indexA, int indexB);

        int altitudeIndex(int index);
        int altitude(int i, int j);


        void setAltitude(float s, int index);

        void affichageGrille() const ;
        void setPcd(int index, float d, float p, string c) const; 

        int getTailleGrille() const;

    private : 
        enum voisins { Ouest = 0, Nord = 1, Est = 2, Sud = 3 };
        int l;
        int c;
        int *tab;
};




#endif