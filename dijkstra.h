#include "graph.h"
#include <iostream>
#include <vector>


struct infoIndex{
    float dist;
    int pred; 
    string couleur;

};


class Dijkstra{
    public :
        Dijkstra(Graph *g);
        ~Dijkstra();

        void dijkstraGraph(Graph *g);
        void affichageTabPcd(Graph *g) const;
        bool present(Graph *g, string col);
        int trouveDepart(int index);
        void voronoi(int libV);


    private :
        infoIndex *dji;
        int tailleGrille;
};






